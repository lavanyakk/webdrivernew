package TestNG;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.Assert;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class VtigerLoginUsingTestNG {
	@Test

	  public void vtigerValidCredentialsTest() {

	      vtigerLogin("crmuser", "crmuser");

	  }

	  @Test

	  public void vtigerInvalidCredentialsTest() {

	      vtigerLogin("crmuser", "crmuserrrrrrrrr");

	  }

	  

	  public void vtigerLogin(String username, String password){

	    Reporter.log("Starting test ...");

	    System.setProperty("webdriver.chrome.driver" ,"./lib/chromedriver.exe");

	    WebDriver driver = new ChromeDriver();

	    driver.manage().window().maximize();

	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	    driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

	    

	    Reporter.log("Navigating to url ..");

	    driver.get("http://encreo.com/crm/");

	    

	    Reporter.log("Entering username");

	    driver.findElement(By.xpath("//input[@name='username']")).sendKeys(username);

	    

	    Reporter.log("Entering password");

	    driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);

	    

	    Reporter.log("Clicking Singin button");

	    driver.findElement(By.xpath("//button[text()='Sign in']")).click();

	    

	    String actual = driver.getTitle();

	    Reporter.log("Actual title of the page \""+ actual + "\"");

	    String expected = "Home";

	    Reporter.log("Verifying the actual title of the page \""+ actual + "\" with expected \"" + expected + "\"");


	    Assert.assertEquals(actual, expected, "When login is successful, the page title should be \""+ expected + "\"");

	    Reporter.log("Ending test ...");


	  }

	}


