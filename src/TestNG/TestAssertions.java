package TestNG;

import org.testng.annotations.Test;
import org.testng.Assert;


public class TestAssertions {
  
	
	@Test

	  public void verifyStringsAreEqual() {

	      System.out.println("Test one begins...");

	      Assert.assertEquals("bharath", "bharath");

	      System.out.println("Test one ends...");
	      System.out.println("Hello");


	  }
  

	  @Test

	  public void verifyNumbersAreEqual() {

	      System.out.println("Test two begins...");

	      Assert.assertEquals(10, 10);


	      System.out.println("Test two ends...");


	  }

	 

	  @Test

	  public void verifyBooleanValuesAreEqual() {

	      System.out.println("Test three begins...");

	      Assert.assertEquals(true, false);

	      System.out.println("Test three ends...");


	  }

	}


