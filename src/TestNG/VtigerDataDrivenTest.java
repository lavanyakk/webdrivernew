package TestNG;

import org.testng.Reporter;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

public class VtigerDataDrivenTest {
	 @Test(dataProvider = "testdata")

	  public void testone(String firstname, String lastname, Integer age) {

	      Reporter.log("Iteration begins");

	      

	      Reporter.log("First name :" + firstname);

	      Reporter.log("Last name :" + lastname);

	      Reporter.log("Age is " + age + " years.");

	      

	      printWelcomeMessage(firstname, lastname, age);

	      Reporter.log("Iteration ends");


	      

	  }


	  @DataProvider

	  public Object[][] testdata() {

	    return new Object[][] {

	      new Object[] { "ram", "c" , 25},

	      new Object[] { "raju", "b", 32 },

	      new Object[] { "rajni", "kant", 2000 },

	    };

	  }

	  

	  

	  public void printWelcomeMessage(String fname, String lname, Integer age){

	      Reporter.log("Hello \""+ lname + ", "+  fname + ", thank you for signing in...");

	      

	  }

	}

