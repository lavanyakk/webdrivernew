

package session2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;

public class methodtesting {

static WebDriver driver ; // this is class level variable

public static void main(String[] args) {

//save the product

// Logs into the vtiger crm application

login();

// navigates to create product page from home page

goToCreateProductsPage();

// enter product name, product price, choose product as active, choose taxes and

createProduct();

//verify the information displayed in View Product Details page

verifyProductInformation();

//logs out of the application

logout();

}

// Logs into the vtiger crm application

private static void login() {


//ChromeDriver driver = new ChromeDriver(); // this is a local variable

 System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");

driver = new ChromeDriver();

driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

driver.get("http://encreo.com/crm");

driver.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");

driver.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");

driver.findElement(By.xpath("//button[text()='Sign in']")).click();

System.out.println("login done");

}

// navigates to create product page from home page

private static void goToCreateProductsPage() {

driver.findElement(By.xpath("(//strong[text()='Products'])[1]")).click();

System.out.println("On products home page");

driver.findElement(By.xpath("//strong[text()='Add Product']")).click();

System.out.println("On Create Product page");

}

// enter product name, product price, choose product as active, choose taxes and save

//the product

private static void createProduct() {

String productNameTextbox = "//input[@name='productname']";

driver.findElement(By.xpath(productNameTextbox)).sendKeys("vtiger crm product 1.2");

driver.findElement(By.xpath("//input[@type='checkbox'][@name='discontinued']")).click();

driver.findElement(By.xpath("//input[@name='unit_price']")).sendKeys("99");

driver.findElement(By.xpath("//input[@type='checkbox'][@name='tax1_check']")).click();

driver.findElement(By.xpath("//input[@type='checkbox'][@name='tax2_check']")).click();

driver.findElement(By.xpath("//input[@type='checkbox'][@name='tax3_check']")).click();

driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys("This is the latest product version");



driver.findElement(By.xpath("(//strong[text()='Save'])[2]")).click();

System.out.println("Save Product button is clicked");

}

private static void verifyProductInformation() {

//System.out.println("To be done");
String savedProductName =driver.findElement(By.xpath("//span[@class='productname']")).getText();
System.out.println("Product name displayed ::"+  savedProductName); }


private static void logout() {

driver.findElement(By.xpath("//strong[text()='Demo']")).click();

driver.findElement(By.xpath("(//a[text()='Sign Out'])[1]")).click();

System.out.println("Signout is done");

} 
}
