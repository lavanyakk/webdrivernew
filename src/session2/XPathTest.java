package session2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

public class XPathTest {


	public static void main(String[] args) throws InterruptedException {
		
	     //System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
	     FirefoxDriver firefox = new FirefoxDriver();	     
	     
	     firefox.manage().window().maximize();
	     firefox.get("http://encreo.com/crm");
		   //enter user name in username textbox

	     firefox.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");
	     
	   
	     // enter password in password textbox

	     firefox.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");

	     // click on signin button

	     firefox.findElement(By.xpath("//button[text()='Sign in']")).click();

	     //click on Leads menu link

	     firefox.findElement(By.xpath("//a[@id='menubar_item_Leads']")).click();

	     //  alternatively, try this also  :   //strong[text()='Leads']

	     // click on Add Lead button

	     firefox.findElement(By.xpath("//strong[text()='Add Lead']")).click();

	     //enter lead first name in lead first name textbox

	     firefox.findElement(By.xpath("//input[@name='firstname']")).sendKeys("Sanjay");

	     // alternatively, you can try : //input[@id='Leads_editView_fieldName_firstname']

	     // enter  lead last name in lead lastname textbox

	     firefox.findElement(By.xpath("//input[@id='Leads_editView_fieldName_lastname']")).sendKeys("Sharma");

	     // alternatively, you can try : //input[@name='lastname']

	     // click on save button

	     firefox.findElement(By.xpath("//strong[text()='Save']")).click();

	     //fetch the full name text from the header element

	     String fullName=firefox.findElement(By.xpath("//h4")).getText();

	     // use getText on an element, to fetch the text from the element. 

	     // stored into a variable , fullName as above. text is of String data type, hence the 

	     //variable type is String

	     // print full name

	     System.out.println("Full name ::"+ fullName);

	     //click on User Name menu element

	     firefox.findElement(By.xpath("//strong[text()='Demo']")).click();

	     // click sign out link
	     firefox.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	     //Delay to take action for the next step
	     firefox.findElement(By.xpath("//a[text()='Sign Out']")).click();

	     // close the window
          Thread.sleep(5000);
          firefox.close();

	     //stop driver

          firefox.quit();
	     

	}

}
