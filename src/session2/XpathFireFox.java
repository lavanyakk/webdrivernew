
package session2;//// change the package name to your package name

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.firefox.MarionetteDriver;

import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;

public class XpathFireFox {

public static void main(String[] args) throws InterruptedException {

// comment these two lines if you are using Firefox 43. This is for firefox 47

//System.setProperty("webdriver.gecko.driver", ".\\drivers\\geckodriver.exe");

//MarionetteDriver firefox= new MarionetteDriver();

FirefoxDriver driver = new FirefoxDriver();

//maximize the window

driver.manage().window().maximize();

//set the page load to wait until finishes or times out

driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

//set the element waiting time out to 20 seconds

driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

// go to vtiger crm login page

driver.get("http://encreo.com/crm");

//enter user name in username textbox

driver.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");

// enter password in password textbox

driver.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");

// click on signin button

driver.findElement(By.xpath("//button[text()='Sign in']")).click();

//click on Leads menu link

driver.findElement(By.xpath("(//strong[text()='Leads'])[1]")).click();

// alternatively, try this also : //strong[text()='Leads']

// click on Add Lead button

driver.findElement(By.xpath("//strong[text()='Add Lead']")).click();

//enter lead first name in lead first name textbox

driver.findElement(By.xpath("//input[@name='firstname']")).sendKeys("Sanjay");

// alternatively, you can try : //input[@id='Leads_editView_fieldName_firstname']

// enter lead last name in lead lastname textbox

driver.findElement(By.xpath("//input[@id='Leads_editView_fieldName_lastname']")).sendKeys(" Sharma");

// alternatively, you can try : //input[@name='lastname']

// click on save button

driver.findElement(By.xpath("//strong[text()='Save']")).click();

//fetch the full name text from the header element

String fullName=driver.findElement(By.xpath("//h4")).getText();

// use getText on an element, to fetch the text from the element.

// stored into a variable , fullName as above. text is of String data type, hence the

//variable type is String

// print full name

System.out.println("Full name ::"+ fullName);

//click on User Name menu element

driver.findElement(By.xpath("//strong[text()='Demo']")).click();

//a[@id='menubar_item_right_Demo']

////strong[text()='Demo']

//sleep for 3000 milliseconds, 3 seconds

//Thread.sleep(3000);

// tell driver to wait for the element to be visible within the timeout. WebDriverWait waiter = new WebDriverWait(driver, 20);

//waiter.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Sign

//Out']")));

// click signout link

driver.findElement(By.xpath("//a[text()='Sign Out']")).click();

// close the window

driver.close();

//stop driver

driver.quit();

}

}