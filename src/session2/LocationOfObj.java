package session2;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocationOfObj {

	public static void main(String[] args) {
		
	     System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
	     ChromeDriver googlechrome = new ChromeDriver();
	     googlechrome.get("http://linkedin.com");
	     
	     googlechrome.findElement(By.name("firstName")).sendKeys("charlie-FirstName");
	     googlechrome.findElement(By.id("last-name")).sendKeys("Chaplin-LastName");
	     googlechrome.findElement(By.className("cell-body-textinput").name("password")).sendKeys("ABC2020");
	     googlechrome.findElement(By.tagName("input").id("login-email")).sendKeys("Found-EMail");
	     googlechrome.findElement(By.linkText("Help Center")).click();
	     googlechrome.findElement(By.partialLinkText("Changing")).click();

	     
	   //*[@id="first-name"]  -- x path
	     //#first-name  -- CSS Path
	     //googlechrome.findElement(By.xpath("/help/linkedin/suggested/267")).click();
	}

}
