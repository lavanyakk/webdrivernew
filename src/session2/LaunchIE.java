package session2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import org.openqa.selenium.ie.InternetExplorerDriver;

public class LaunchIE {


	public static void main(String[] args) {


	    System.setProperty("webdriver.ie.driver", "./lib/IEDriverServer.exe");
	    InternetExplorerDriver driver = new InternetExplorerDriver();
	    driver.get("https://www.linkedin.com/");
	    
	    driver.manage().window().maximize(); //This will maximize the window

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		
	    driver.findElement(By.id("first-name")).sendKeys("bunny");
	    driver.findElement(By.name("lastName")).sendKeys("abc");
	}

}






