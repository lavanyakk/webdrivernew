package session2;

	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.By;

	import org.openqa.selenium.WebDriver;

	import org.openqa.selenium.WebElement;

    import org.openqa.selenium.chrome.ChromeDriver;
    

	public class VerifyLoginSuccessful {

	private static String xpath_usernametextbox = "//input[@name='username']";

	private static String xpath_passwordtextbox = "//input[@name='password']";

	private static String xpath_signinButton = "//button[text()='Sign in']";
	

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./lib/chromedriver.exe");
	 

	WebDriver driver = new ChromeDriver();

	driver.manage().window().maximize();

	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	

	driver.get("http://encreo.com/crm");

	System.out.println("Login page title ::" + driver.getTitle());

	String loginPageTitle = driver.getTitle();
	

	WebElement usernameTextbox =driver.findElement(By.xpath(xpath_usernametextbox));

	usernameTextbox.clear();

	usernameTextbox.sendKeys("crmuser");

	WebElement passwordTextbox = 	driver.findElement(By.xpath(xpath_passwordtextbox));
	passwordTextbox.clear();
	passwordTextbox.sendKeys("crmuser");

	WebElement signinButton = driver.findElement(By.xpath(xpath_signinButton));

	signinButton.click();

	System.out.println("Home page title ::" + driver.getTitle());

	String actualHomePageTitle = driver.getTitle();

	String expectedHomePageTitle = "Home";

	if(actualHomePageTitle.equals(expectedHomePageTitle)){

	System.out.println("Login is successful");

	}else{

	System.err.println("Login is not successful.");

	System.out.println("Expected Page title ::"+ expectedHomePageTitle );

	System.out.println("Actual Page title ::" + actualHomePageTitle);

	}

	}

	}