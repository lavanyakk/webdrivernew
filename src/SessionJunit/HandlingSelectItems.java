package SessionJunit;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.Before;

import org.junit.Test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.Select;

public class HandlingSelectItems {


	WebDriver driver;

	String appURL="http://encreo.com/in/contact.html";

	String xpath_SalutationListbox = "//select[@name='salutation']";
	
	
	
	@Before

	public void setUp() throws Exception {

	System.setProperty("webdriver.chrome.driver" ,"./lib/chromedriver.exe");

	driver = new ChromeDriver();

	driver.manage().window().maximize();

	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

	driver.get(appURL);

	}
	
	@Test

	public void test() {

	// fetch the web element using driver.findElement method

	WebElement listElement =driver.findElement(By.xpath(xpath_SalutationListbox));

	//listElement.clear();

	//listElement.sendKeys("");

	//listElement.click();

	//create a SELECT class object by passing the web element

	Select selectElement = new Select(listElement);

	String optionSelectedFromList=selectElement.getFirstSelectedOption().getText();

	System.out.println(optionSelectedFromList);// this will show Salutation
	selectElement.selectByVisibleText("Dr."); // this uses the text of the option to be chosen
	
	
	optionSelectedFromList=selectElement.getFirstSelectedOption().getText();

	System.out.println(optionSelectedFromList);// this will show Dr.
	
	selectElement.selectByIndex(2);//this uses the position of the option to be chosen
	// this will show Ms. as the index starts from 0 , 3rd item in the drop down list.
	
	optionSelectedFromList=selectElement.getFirstSelectedOption().getText();

	System.out.println(optionSelectedFromList);// this will show Ms. as the index starts from 0 , 3rd item in the drop down list.
	selectElement.selectByValue("Frau");// this uses the value attribute of the option to be chosen
	
	optionSelectedFromList=selectElement.getFirstSelectedOption().getText();

	System.out.println(optionSelectedFromList); // this will show Frau
	
	int selectedItemCOunt = selectElement.getAllSelectedOptions().size();

	System.out.println(selectedItemCOunt); // this will show only 1
	int itemCount = selectElement.getOptions().size();

	System.out.println(itemCount);// this will show 8
	
	selectElement.selectByValue("Prof.");
	optionSelectedFromList=selectElement.getFirstSelectedOption().getText();
	System.out.println(optionSelectedFromList); 

	
	selectElement.selectByIndex(4);
	optionSelectedFromList=selectElement.getFirstSelectedOption().getText();
	System.out.println(optionSelectedFromList); 


}
	// Try South West Drop Down
}