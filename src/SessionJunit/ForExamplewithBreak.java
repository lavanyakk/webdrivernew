package SessionJunit;



import static org.junit.Assert.*;


import java.util.ArrayList;

import java.util.HashSet;

import java.util.List;

import java.util.Set;


import org.junit.After;

import org.junit.Before;
import org.junit.Test;

public class ForExamplewithBreak {

	@Test

    public void test() {

        

        int[] arrayOfIntegers = {10, 29, 30, 49, 50};

        

        List<String>  names =  new ArrayList<String>();

        

        names.add("bharath");

        names.add("surya");

        names.add("dave");

        names.add("debs");

        

        Set<String> places  =  new HashSet<String>();

        

        for(int myInteger: arrayOfIntegers){

            System.out.println(myInteger); // iterating over an array of Integers

        }

        

        for(int counter=0; counter<=5;counter++){  // iterating incrementally between 0 to 5

            System.out.println(counter);

        }

        

        int x= 10;

        for(int counter=x; counter<=50;counter++){  // iterating incrementally between 0 to 5

            System.out.println(counter);

            if(counter ==40){

                break;

            }

        }

        

        for(String name : names){  // iterating over a list of Strings

            System.out.println(name);

        }

    }


}
