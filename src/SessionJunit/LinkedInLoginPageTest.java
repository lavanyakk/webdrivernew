package SessionJunit;


import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;


import org.junit.After;

import org.junit.AfterClass;

import org.junit.Before;

import org.junit.Test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.Assert;


public class LinkedInLoginPageTest {

WebDriver driver ;

@Before

public void setUp(){

System.setProperty("webdriver.chrome.driver" ,"./lib/chromedriver.exe");

driver = new ChromeDriver();

driver.manage().window().maximize();

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

driver.get("https://www.linkedin.com/");

}

@Test

public void linkTests() {

/* System.setProperty("webdriver.chrome.driver" ,"./drivers/chromedriver");

driver = new ChromeDriver();

driver.manage().window().maximize();

driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

driver.get("https://www.linkedin.com/");*/

//forgot password link should be displayed

String xpath_ForgotPasswordLink = "//a[text()='Forgot password?']";

WebElement forgotPasswordLink =

driver.findElement(By.xpath(xpath_ForgotPasswordLink));

Assert.assertTrue(forgotPasswordLink.isDisplayed());

//OR

boolean isLinkDisplayed = forgotPasswordLink.isDisplayed();

Assert.assertEquals(isLinkDisplayed, true);

}

@After

public void finish(){

driver.close();

driver.quit();

}

@Test

public void loginTest() {

// create account button should be displayed

String xpath_CreateAccountButton = "//span[text()='Create account']";

WebElement createAccountButton =

driver.findElement(By.xpath(xpath_CreateAccountButton));

Assert.assertTrue(createAccountButton.isDisplayed());

//OR

boolean isButtonDisplayed = createAccountButton.isDisplayed();

Assert.assertEquals(isButtonDisplayed, true);

}

@Test

public void logoTest() {

//logo should be displayed

String xpath_LinkedinLogo = "//span[text()='Create account']";

WebElement linkedinLogo = driver.findElement(By.xpath(xpath_LinkedinLogo));

Assert.assertTrue(linkedinLogo.isDisplayed());

//OR

boolean isLogoDisplayed = linkedinLogo.isDisplayed();

Assert.assertEquals(isLogoDisplayed, true);

}

}