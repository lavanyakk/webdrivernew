package SessionJunit;

import static org.junit.Assert.*;

import org.junit.Test;


import static org.junit.Assert.*;


import java.util.Set;

import java.util.concurrent.TimeUnit;


import org.junit.After;

import org.junit.Before;



import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingWindows {

	
		String appURL = "http://encreo.com";

	    String xpath_SigninLink ="//a[.='Sign in']";

	    String xpath_UsernameTextbox ="//input[@placeholder= 'User name']";

	    String xpath_PasswordTextbox ="//input[@placeholder= 'Password']";

	    String xpath_SignInbutton ="//button[.='Log In']";

	    String password ="wrongpasswrod";

	    String username ="admin@encreo.com";



	    @Test

	    public void test() {

	        

	        System.setProperty("webdriver.chrome.driver" ,"./lib/chromedriver.exe");

	        WebDriver driver = new ChromeDriver(); 

	        driver.manage().window().maximize();

	        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

	        driver.get(appURL);

	        

	        System.out.println("clicking on signin link");

	        driver.findElement(By.xpath(xpath_SigninLink)).click();

	        

	        // the error happened here, is that webdriver is looking for username textbox in the default window

	        // so we need to tell webdriver to switch to the second window. and then search for the elements.

	        

	        Set<String> winHandles = driver.getWindowHandles();

	        int windowsPresent = driver.getWindowHandles().size();

	        System.out.println("Window count ::"+ windowsPresent);

	        

	        for(String winHandle : winHandles){

	            System.out.println(winHandle);

	            driver.switchTo().window(winHandle);

	            System.out.println("switched to window with handle ::"+ winHandle);

	            System.out.println("Window title ::"+ driver.getTitle());

	            System.out.println("Window current URL  ::"+ driver.getCurrentUrl());

	            if(driver.getTitle().equals("Encreo Test Studio")){

	                break;

	            }

	        }

	        

	        System.out.println("signing in....");

	        driver.findElement(By.xpath(xpath_UsernameTextbox)).sendKeys(username);

	        driver.findElement(By.xpath(xpath_PasswordTextbox)).sendKeys(password);

	        driver.findElement(By.xpath(xpath_SignInbutton)).click();


	        

	    }


	}

