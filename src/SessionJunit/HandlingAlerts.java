package SessionJunit;

import static org.junit.Assert.*;

import org.junit.Test;


import static org.junit.Assert.assertTrue;


import java.util.concurrent.TimeUnit;


import org.junit.After;

import org.junit.Before;



import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.Assert;

public class HandlingAlerts {

	

	    WebDriver driver ;

	    

	    //test data variables 

	    String appURL  = "http://encreo.com/crm/index.php";

	    String username = "crmuser";

	    String password ="crmuser";

	    

	    String productName = "CRM Version 5.x";

	    String productPrice = "129.00";

	    //xpaths

	    String xpath_UsernameTextbox="//input[@type='text'][@placeholder='Username']";

	    String xpath_PasswordTextbox ="//input[@type='password'][@placeholder='Password']";

	    String xpath_SignInButton = "//button[text()='Sign in']";


	    

	    String xpath_ProductsTab  = "//strong[text()='Products']";

	    String xpath_AddProductButton  = "//button[contains(.,'Add Product')]";

	    String xpath_ProductnameTextbox = "//input[@type='text'][@name='productname']";

	    

	    String xpath_ProductPriceTextbox = "//input[@type='text'][@name='unit_price']";

	    String xpath_SaveButton = "//strong[text()='Save']";

	    String xpath_CancelLink= "//a[text()='Cancel']";

	    String xpath_UserMenuLink   = "//strong[text()='Demo']";

	    String xpath_LogoutLink  = "//a[text()='Sign Out']";

	    

	    @Before

	    public void login()  {

	        System.setProperty("webdriver.chrome.driver" ,"./lib/chromedriver.exe");

	        driver = new ChromeDriver(); 

	        driver.manage().window().maximize();

	        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

	        driver.get(appURL);

	        

	        driver.findElement(By.xpath(xpath_UsernameTextbox)).sendKeys(username);

	        driver.findElement(By.xpath(xpath_PasswordTextbox)).sendKeys(password);

	        driver.findElement(By.xpath(xpath_SignInButton)).click();

	    }

	    


	    @After

	    public void logout() {

	        driver.findElement(By.xpath(xpath_UserMenuLink)).click();

	        driver.findElement(By.xpath(xpath_LogoutLink)).click();

	    }


	    @Test

	    public void createProductTest() throws InterruptedException {

	        

	        driver.findElement(By.xpath(xpath_ProductsTab)).click();

	        driver.findElement(By.xpath(xpath_AddProductButton)).click();

	        driver.findElement(By.xpath(xpath_ProductnameTextbox)).sendKeys(productName);

	        driver.findElement(By.xpath(xpath_ProductPriceTextbox)).sendKeys(productPrice);

	        driver.findElement(By.xpath(xpath_CancelLink)).click();

	        

	        Thread.sleep(5000);

	        String alertMessage  = driver.switchTo().alert().getText();

	        System.out.println("Message from the alert::"+ alertMessage);

	        //driver.switchTo().alert().accept(); // ok button, yes, stay , the default button

	        //System.out.println(" page when Leave clicked ::" +driver.getTitle());

	        driver.switchTo().alert().dismiss();  // cancel button, no, leave...the non-default button

	        System.out.println(" page when Stay clicked ::" +driver.getTitle());



	    }





	}

